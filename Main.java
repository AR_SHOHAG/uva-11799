import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T, N, k, c, s=0;
		T=in.nextInt();

	    if(T<=50){
	        for(c=1; c<=T; ++c){
	            N=in.nextInt();
	            int i[] = new int[N];
	            for(k=0; k<N; ++k){
	                i[k]=in.nextInt();
	                if(i[k]>s){
	                    s=i[k];
	                }
	            }
	            System.out.println("Case " + c + ": " + s);
	            s=0;
	        }
	    }
	}

}
